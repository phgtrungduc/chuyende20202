$(document).ready(function () {
    new BaseJS();
    var role = localStorage.getItem("Role");
    getData(init, 'all');
});


var data = [];
// Lấy dữ liệu từ api về 
// Thêm URL 
function getData(myCallback, option) {
    var me = this;
    // Thêm URL
    let role = JSON.parse(localStorage.getItem('Role'));
    var url = "../api/v1/text";
    if (role == 0) {
        url = "../api/v1/text/alltext";
    }

    if (role == 2) {
        let _id = localStorage.getItem('EmployeeId');
        url = "../api/v1/employee/assignto/"+_id; 
    }
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("accessToken"));
    xhr.onload = function () {
        if (xhr.readyState == 4 && xhr.status == "200") {
            var _data = JSON.parse(xhr.responseText);
            me.data = _data;
            myCallback(_data, option);
        } else {
            notification("Tải dữ liệu thất bại", "fail");
        }
    }
    xhr.send(null);
}

// Khởi tạo
function init(_data, option = 'all') {
    var me = this;
    let data = [];
    switch (option) {
        case 'accepted':
            data = _data.filter(data => data.Status == 1);
            break;
        case 'not-approved':
            data = _data.filter(data => data.Status == 0);
            break;
        case 'rejected':
            data = _data.filter(data => data.Status == 2);
            break;
        default:
            data = _data;
            break;
    }

    let role = JSON.parse(localStorage.getItem('Role'));
    // let role = 0;

    let count_item = data.length;
    let html = ``;
    if (count_item>0){
        if (role > 0) {
            for (let i = 0; i < count_item; i++) {
                html += `
                <tr>
                    <td>${data[i].EmployeeFullName}</td>
                    <td>${data[i].TypeText}</td>
                    <td>${data[i].AssignToFullName}</td>
                    <td>${data[i].CreatedDate}</td>
                `;
    
                if (data[i].Status == 1) {
                    html += `
                        <td class="accepted" id="status-${data[i].TextId}">Chấp nhận</td>
                    `
                } else if (data[i].Status == 0) {
                    html += `
                        <td class="not-approved" id="status-${data[i].TextId}">Chưa duyệt</td>
                    `
                } else {
                    html += `
                        <td class="rejected" id="status-${data[i].TextId}">Từ chối</td>
                    `
                }
    
                html += `
                    <td>
                        <p id="show-${data[i].TextId}"><i class="fas fa-angle-down detail" onclick="show_detail('${data[i].TextId}')"></i></p>
                        <p id="hide-${data[i].TextId}" class="hide-detail"><i class="fas fa-angle-left detail" onclick="hide_detail('${data[i].TextId}')"></i></p>
                    </td>
                </tr>
                <tr id="detail-${data[i].TextId}" class="detail-box">
                    <td colspan="6" class="detail-info">
                        
                        <ul>
                            <li class="info-item"><strong>Ngày bắt đầu:</strong> ${data[i].StartDate}</li>
                            <li class="info-item"><strong>Ngày kết thúc:</strong> ${data[i].ToDate}</li>
                            <li class="info-item"><strong>Nội dung:</strong> ${data[i].Content}</li>
                        </ul>
                `;
    
                if (role == 2) {
                    html += `
                        <p style="text-align: end;" id="btns-${data[i].TextId}">
                            <button class="m-btn primary-button" style="display: inline;" onclick="accept(${i})">Chấp nhận</button>
                            <button class="m-btn warning-button" style="display: inline;" onclick="reject(${i})">Từ chối</button>
                        </p>
                    `;
                }
    
                html += `
                    
                    </td>
                </tr>
                `;
            }
        } else {
    
            let EmployeeCode = localStorage.getItem('EmployeeCode');
            for (let i = 0; i < count_item; i++) {
                if (EmployeeCode == data[i].EmployeeCode) {
                    html += `
                    <tr>
                        <td>${data[i].EmployeeFullName}</td>
                        <td>${data[i].TypeText}</td>
                        <td>${data[i].AssignToFullName}</td>
                        <td>${data[i].CreatedDate}</td>
                    `;
    
                    if (data[i].Status == 1) {
                        html += `
                            <td class="accepted" id="status-${data[i].TextId}">Chấp nhận</td>
                        `
                    } else if (data[i].Status == 0) {
                        html += `
                            <td class="not-approved" id="status-${data[i].TextId}">Chưa duyệt</td>
                        `
                    } else {
                        html += `
                            <td class="rejected" id="status-${data[i].TextId}">Từ chối</td>
                        `
                    }
    
                    html += `
                        <td>
                            <p id="show-${data[i].TextId}"><i class="fas fa-angle-down detail" onclick="show_detail('${data[i].TextId}')"></i></p>
                            <p id="hide-${data[i].TextId}" class="hide-detail"><i class="fas fa-angle-left detail" onclick="hide_detail('${data[i].TextId}')"></i></p>
                        </td>
                    </tr>
                    <tr id="detail-${data[i].TextId}" class="detail-box">
                        <td colspan="6" class="detail-info">
                            
                            <ul>
                                <li class="info-item"><strong>Ngày bắt đầu:</strong> ${data[i].StartDate}</li>
                                <li class="info-item"><strong>Ngày kết thúc:</strong> ${data[i].ToDate}</li>
                                <li class="info-item"><strong>Nội dung:</strong> ${data[i].Content}</li>
                            </ul>
                    `;
    
                    if (data[i].Status == 0) {
                        html += `
                            <p style="text-align: end;" id="btns-${data[i].TextId}">
                                <button class="m-btn m-btn-delete" style="display: inline;" onclick="cancle('${data[i].TextId}')">Hủy</button> 
                            </p>
                        `;
                    }
    
                    html += `
                        </td>
                    </tr>
                    `;
                }
            }
        }
    }else {
        html += `<div>Không có đơn từ nào</div>`
    }



    
    document.getElementsByClassName("table-content")[0].innerHTML = html;
}

// Lọc đơn theo trang thái : tất cả, chấp nhận, chưa duyệt, từ chối.
function filter() {
    var x = document.getElementById("cbxPosition").value;
    getData(init, x);
}

// Reload
function reload() {
    filter();
}

// Xem chi tiết
function show_detail(TextId) {
    let detail = 'detail-' + TextId;
    let _show = 'show-' + TextId;
    let _hide = 'hide-' + TextId;
    document.getElementById(detail).classList.add('show-detail');
    document.getElementById(_hide).classList.remove('hide-detail');
    document.getElementById(_show).classList.add('hide-detail');
}

// Thu gọn
function hide_detail(TextId) {
    let detail = 'detail-' + TextId;
    let _show = 'show-' + TextId;
    let _hide = 'hide-' + TextId;
    document.getElementById(detail).classList.remove('show-detail');
    document.getElementById(_hide).classList.add('hide-detail');
    document.getElementById(_show).classList.remove('hide-detail');
}

// Chấp nhận đơn từ
function accept(index) {
    this.data[index].Status = 1;
    this.update(this.data[index]);
}

// Từ chối đơn từ
function reject(index) {
    this.data[index].Status = 2;
    this.update(this.data[index]);
}

// Update trạng thái của đơn từ
// Thêm URL
function update(data) {
    // Thêm URL
    var url = "../api/v1/text/";
    var form_data = new FormData();
    for (var key in data) {
        form_data.append(key, data[key]);
    };
    $.ajax({
        url: url + data.TextId,
        data: form_data,
        type: 'PUT',
        processData: false,
        contentType: false,
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem("accessToken"),
        },
    }).done(function (data) {
        reload();
        notification("Thành công", "success");
    }).fail(function (data) {
        reload();
        notification("Thất bại", "fail");
    })
};


// Hủy đơn 
// Thêm URL 
function cancle(TextId) {
    // Thêm URL 
    var url = "../api/v1/text/";
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", url + TextId, true);
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("accessToken"));
    xhr.onload = function () {
        if (xhr.readyState == 4 && xhr.status == "200") {
            reload();
            notification("Hủy đơn thành công", "success");
        } else {
            notification("Hủy đơn thất bại", "fail");
        }
    }
    xhr.send(null);
}

// Hàm thông báo
function notification(content, status) {
    $(".notification").show();
    //Đảm bảo nếu có thông báo liên tục sẽ đè thông báo sau lên thông báo trước luôn
    $(".notification-content").empty();
    let notificationGroup = $(".notification-group");
    $(notificationGroup).empty();
    if (status === "success") {
        let notification = $(`<div class="notification notification-success">`);
        let icon = `<div class="notification-icon notification-icon-success"></div>`;
        let contentTitle = `<div class="notification-content">${content}</div>`;
        $(notification).append(icon);
        $(notification).append(contentTitle);
        $(notificationGroup).append(notification);
    } else if (status === "fail") {
        let notification = $(`<div class="notification notification-danger">`);
        let icon = `<div class="notification-icon notification-icon-danger"></div>`;
        let contentTitle = `<div class="notification-content">${content}</div>`;
        $(notification).append(icon);
        $(notification).append(contentTitle);
        $(notificationGroup).append(notification);
    }
}

// Lấy dữ liệu từ cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// Hiển thị form 
function addEntity() {
    $(".include-content").show();
    // Lấy danh sách đơn
    getTypeText();
    // Lấy danh sách người phê duyệt
    getAssignTo();
    // Gán datepicker cho các input date
    setDatePicker();
    let allInput = $("[inputField]");
    $(allInput).val(null);
    $(allInput).removeClass("border-red");
}

// Ẩn form
function hideDialog() {
    $(".include-content").hide();
}

// Gán datepicker cho các input date
function setDatePicker() {
    let datepicker = $("[type='datepicker']");
    $(datepicker).datepicker({ dateFormat: "dd/mm/yy" });
}

// Kiểm tra: ToDate > StartDate
function setToDate() {
    let datepicker = $("[type='datepicker']");
    let startDate = $(datepicker[0]).val();
    startDate = startDate.split('/');
    startDate = new Date(startDate[2], startDate[1], startDate[0]);
    let toDate = $(datepicker[1]).val();
    toDate = toDate.split('/');
    toDate = new Date(toDate[2], toDate[1], toDate[0]);
    if (toDate.getTime() <= startDate.getTime()) {
        $(datepicker[1]).addClass("border-red");
        notification("Ngày kết thúc phải sau ngày bắt đầu", "fail");
    } else {
        $(datepicker[1]).removeClass("border-red");
    }
}

// Tạo đơn mới
// Thêm URL
function saveText() {
    let allInput = $("[inputField]");
    let flag = true;
    let data = {};
    $.each(allInput, function (index, input) {
        // console.log($(input).val());
        if (!$(input).val() || $(input).val() == '') {
            flag = false;
            $(input).addClass("border-red");
            notification("Chưa điền đủ các trường bắt buộc", "fail");
            return false;
        }
        data[$(input).attr("inputField")] = $(input).val();
    });

    let datepicker = $("[type='datepicker']");
    let startDate = $(datepicker[0]).val();
    startDate = startDate.split('/');
    startDate = new Date(startDate[2], startDate[1] - 1, startDate[0]);
    let toDate = $(datepicker[1]).val();
    toDate = toDate.split('/');
    toDate = new Date(toDate[2], toDate[1] - 1, toDate[0]);

    if (toDate.getTime() <= startDate.getTime()) {
        $(datepicker[1]).addClass("border-red");
        notification("Ngày kết thúc phải sau ngày bắt đầu", "fail");
        flag = false;
    }
    data.StartDate = startDate.toUTCString();
    data.ToDate = toDate.toUTCString();

    if (flag) {
        data.Status = 0;
        let employeeCode = localStorage.getItem('EmployeeCode');
        data.EmployeeCode = employeeCode;
        console.log(data);

        // Thêm URL
        let url = "http://localhost:51981/api/v1/text";

        var form_data = new FormData();
        for (var key in data) {
            form_data.append(key, data[key]);
        };

        $.ajax({
            url: url,
            data: form_data,
            type: 'POST',
            processData: false,
            contentType: false,
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem("accessToken"),
            },
        }).done(function (data) {
            reload();
            notification("Tạo đơn thành công! Đơn đang chờ duyệt.", "success");
            $(".include-content").hide()
        }).fail(function (data) {
            reload();
            notification("Tạo đơn thất bại!", "fail");
            $(".include-content").hide()
        })
    }

}

// Lấy danh sách đơn
// Thêm URL
// Sử dụng 2 trường: TextId và TextName (sửa lại theo dữ liệu api trả về)
function getTypeText() {
    // Thêm URL
    var url = "http://localhost:51981/api/v1/typetext";
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("accessToken"));
    xhr.onload = function () {
        var typeTexts = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            let html = `
                <div class="m-label">Loại đơn (<span style="color:red;">*</span>)</div>
                <select id="slTypeText" type="select" class="m-control" inputField="TypeText"
                    selectField="TypeText" resField="TypeText">
            `;
            for (let i = 0; i < typeTexts.length; i++) {
                html += `
                    <option value="${typeTexts[i].TypeTextCode}">
                        ${typeTexts[i].TypeTextName}
                    </option>
                `;
            }

            html += `
                </select>
            `;

            document.getElementById('_TypeText').innerHTML = html;

        } else {
            notification(" Lỗi tải dữ liệu!", "fail");
        }
    }
    xhr.send(null);
}

// Lấy danh sách người phê duyệt
// Thêm URL
// Sử dụng 2 trường: EmployeeId và EmployeeName (sửa lại theo dữ liệu api trả về)
function getAssignTo() {
    // Thêm URL
    var url = "http://localhost:51981/api/v1/employee/byrole/3";
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("accessToken"));
    xhr.onload = function () {
        var assignTos = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            let html = `
                <div class="m-label">Người phê duyệt (<span style="color:red;">*</span>)</div>
                <select id="alAssignTo" type="select" class="m-control" inputField="AssignTo"
                    selectField="AssignTo" resField="AssignTo">
            `;
            for (let i = 0; i < assignTos.length; i++) {
                html += `
                    <option value="${assignTos[i].EmployeeId}">
                        ${assignTos[i].FullName}
                    </option>
                `;
            }

            html += `
                </select>
            `;

            document.getElementById('_AssignTo').innerHTML = html;

        } else {
            notification(" Lỗi tải dữ liệu!", "fail");
        }
    }
    xhr.send(null);
}