function LoginFormm (options) {

    function getParent(element, selector) {
        while (element.parentElement) {
            if (element.parentElement.matches(selector)) {
                return element.parentElement;
            }
            element = element.parentElement;
        }
    }

    var selectorRules = {};

    function validate(inputElement, rule) {
        var errorMessage;
        var errorElement = getParent(inputElement,options.formGroupSelector).querySelector(options.errorSelector);

        var rules = selectorRules[rule.selector];

        for (var i = 0;i <rules.length; i++) {
            errorMessage = rules[i](inputElement.value);
            if (errorMessage) break;
        }

        if (errorMessage) {
            errorElement.innerText = errorMessage;
             getParent(inputElement,options.formGroupSelector).classList.add('invalid');
        } else {
            errorElement.innerText = "";
             getParent(inputElement,options.formGroupSelector).classList.remove('invalid');
        }

        return !errorMessage;
    }

    var formElement = document.querySelector(options.form);

    if (formElement) {
        formElement.onsubmit = function (e) {
            e.preventDefault();

            var isVlaid = true;

            options.rules.forEach(rule => {
                var inputElement = formElement.querySelector(rule.selector);
                var a = validate(inputElement,rule);
                if (!a) {
                    isVlaid = false;
                }
            });

            if (isVlaid) {
                if (typeof options.onSubmit === "function") {

                    var enableInputs = formElement.querySelectorAll('[name]');
                    var formValues = Array.from(enableInputs).reduce(function (values, input) {
                        values[input.name] = input.value;
                        return values;
                    },{});
                    options.onSubmit(formValues);
                } else {
                    formElement.submit();
                }
            }
        }
        options.rules.forEach(rule => {

            if (Array.isArray(selectorRules[rule.selector] )) {
                selectorRules[rule.selector].push(rule.test);
            } else {
                selectorRules[rule.selector] = [rule.test];
            }

            var inputElement = formElement.querySelector(rule.selector);
           
            if (inputElement) {
                inputElement.onblur = function () {
                    validate(inputElement,rule);
                }

                inputElement.oninput = function () {
                    var errorElement =  getParent(inputElement,options.formGroupSelector).querySelector(options.errorSelector);
                    errorElement.innerText = "";
                     getParent(inputElement,options.formGroupSelector).classList.remove('invalid');
                }
            }
        });
    }
}

LoginFormm.isRequired = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            return value.trim() ? undefined : "Vui lòng nhập trường này";
        }
    }
}

LoginFormm.isEmail = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            return regex.test(value) ? undefined : "Trường này phải là email";
        }
    }
}

LoginFormm.minLength = function (selector, min) {
    return {
        selector: selector,
        test: function (value) {
            return value.length >= min ? undefined : `Mật khẩu tối thiểu ${min} kí tự `;
        }
    }
}

LoginFormm.isConfirmed = function (selector, confirmPass, message) {
    return {
        selector: selector,
        test: function (value) {
            return value === confirmPass() ? undefined : message || "Giá trị nhập vào không chính xác";
        }
    }
}