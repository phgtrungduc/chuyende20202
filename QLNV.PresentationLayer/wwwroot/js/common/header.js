$(document).ready(function () {
    initInfo();
    displayPopupDetail();
    logout();
    changePassword();
    handleChangePassword();
});
function initInfo() {
    var avatar = localStorage.getItem("Avatar");
    var fullName = localStorage.getItem("FullName");
    var employeeCode = localStorage.getItem("EmployeeCode");
    $(".EmployeeCodeLogOut").text(employeeCode);
    $("#FullName").text(fullName);
    $(".FullNameLogOut").text(fullName);

    var Role = localStorage.getItem("Role");
    if (Role == "0") {
        Role = "Nhân viên";
    }
    else if (Role == "1") {
        Role = "Quản lý nhân lực";
    }
    else {
        Role = "Giám đốc";
    }
    $(".RoleLogOut").text(Role);
    if (avatar != "null") {
        $("#Avatar").attr("src", "../" + avatar);
    }
    else {
        $("#Avatar").attr("src", "../uploads/avatar/default.png")
    };
}
function displayPopupDetail() {
    var self = this;
    $(".header-right").click(function () {
        var popup = $(".popup-detail");
        if (popup.is(":visible")) {
            popup.hide();
        } else {
            popup.show();
        }
    })
}
function logout() {
    $("#logout").click(function () {
        localStorage.clear();
        window.location.replace("../page/loginform.html");
    });
}
function changePassword() {
    $("#changePassword").click(function () {
        $("#btn-cancle-dialog, #btnCancel").click(function () {

            $(".content-changePassword").hide();
        });
        $(".content-changePassword").show();
        var txtPasswordOld = $("#txtPasswordOld");
        var txtPasswordNew = $("#txtPasswordNew");
        var txtConfirmPassword = $("#txtConfirmPassword");
        txtPasswordOld.removeClass("border-red");
        txtPasswordNew.removeClass("border-red");
        txtConfirmPassword.removeClass("border-red");
        $(".popup-detail").hide();
    });
}
function handleChangePassword() {
    var self = this;

    $("#btnChangePassword").click(function () {
        var txtPasswordOld = $("#txtPasswordOld").val();
        var txtPasswordNew = $("#txtPasswordNew").val();
        var txtConfirmPassword = $("#txtConfirmPassword").val();
        var mess = "";
        if (!txtPasswordOld) {
            self.showNotification("Mật khẩu cũ không được để trống!", "fail");
            return;
        }
        if (!txtPasswordNew) {
            self.showNotification("Mật khẩu mới không được để trống!", "fail");
            return;
        }
        if (!txtConfirmPassword) {
            self.showNotification("Xác nhận mật khẩu không được để trống!", "fail");
            return;
        }
        if (txtPasswordNew != txtConfirmPassword) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: "Mã xác nhận và mật khẩu không khớp!"
            });
            return;
        }
        if (txtPasswordNew==txtPasswordOld){
            Swal.fire({
                icon: 'error',
                title: 'Có vẻ bạn chưa thay đổi mật khẩu...',
                text: "Hãy chọn mật khẩu mới khác với mật khẩu cũ!"
            });
            return;
        }
        var req = JSON.stringify({ oldPass: txtPasswordOld, newPass: txtPasswordNew });
        $.ajax({
            url: "http://localhost:51981/api/v1/login/changepassword",
            method: "POST",
            contentType: "application/json",
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem("accessToken"),
            },
            data: JSON.stringify(req)
        })
            .done(function (res) {
                Swal.fire({
                    icon: 'success',
                    title: 'Chúc mừng!',
                    text: "Bản đã đổi mật khẩu thành công"
                })
                $(".content-changePassword").hide();
            })
            .fail((e) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: e.responseJSON.Messenger
                });
            });
    })
}