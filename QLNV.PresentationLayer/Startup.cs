﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.interfaces;
using QLNV.BusinessLayer.Interfaces;
using QLNV.BusinessLayer.Services;
using QLNV.BusinessLayer.Middwares;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using QLNV.BusinessLayer.Middlewares;
using QLNV.DataLayer;

namespace QLNV.PresentationLayer {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            //services.AddSingleton<IFileProvider>(new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(),"wwwwroot")));
            services.AddControllers()
                //các key của object trả về ở dạng khoogn phân biệt hoa, thường. Cần thêm cái này để viết hoa chuẩn các Key
                .AddNewtonsoftJson(options => {
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

                });
            

            //cors
            services.AddCors();

            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped(typeof(IBaseService<>), typeof(BaseService<>));
             
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IEmployeeService, EmployeeService>();


            services.AddScoped<IPositionRepository, PositionRepository>();
            services.AddScoped<IPositionService, PositionService>();

            services.AddScoped<ILoginRepository, LoginRepository>();
            services.AddScoped<ILoginService, LoginService>();

            services.AddScoped<ITextRepository, TextRepository>();
            services.AddScoped<ITextService, TextService>();

            services.AddScoped<IWorkingDayRepository, WorkingDayRepository>();
            services.AddScoped<IWorkingDayService, WorkingDayService>();

            services.AddScoped<ITypeTextRepository, TypeTextRepository>();
            services.AddScoped<ITypeTextService, TypeTextService>();

            //services.AddScoped<IAuthenticationManager, AuthenticationManager>();
            //add authen 
            var key = "Phương Trung Đức";
            services.AddAuthentication(x => {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddSingleton<IAuthenticationManager>(new AuthenticationManager(key));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            else {
                app.UseHsts();
            }
            

            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseMiddleware<TestMiddleware>();
            app.UseHttpsRedirection();
            app.UseRouting();
            //cors
            app.UseCors(option => option.AllowAnyOrigin().AllowAnyHeader());
            //dùng token
            app.UseAuthentication();
            //Nhận các file static từ wwwroot
            app.UseStaticFiles();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
            app.Run(async (context) => {
                await context.Response.WriteAsync("Đã có lỗi xảy ra");
            });
        }
    }
}
