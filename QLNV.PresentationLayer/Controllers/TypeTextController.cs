﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;

namespace QLNV.PresentationLayer.Controllers {
    /// <summary>
    /// Controller lấy tất cả các vị trí
    /// </summary>
    /// CreatedBy:PTDuc(04/12/2020)
    public class TypeTextController : BaseEntityController<TypeText> {
        ITypeTextService _typeTextService;
        public TypeTextController(ITypeTextService typeTextService) : base(typeTextService) {
            _typeTextService = typeTextService;
        }
    }
}
