﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;

namespace QLNV.PresentationLayer.Controllers {
    /// <summary>
    /// Controller lấy tất cả các vị trí
    /// </summary>
    /// CreatedBy:PTDuc(04/12/2020)
    public class TextController : BaseEntityController<Text> {
        ITextService _textService;
        public TextController(ITextService textService) : base(textService) {
            _textService = textService;
        }
        [HttpGet("alltext")]
        public IActionResult GetTextByEmployeeCode() {
            string employeeCode = null;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null) {
                IEnumerable<Claim> claims = identity.Claims;
                employeeCode = identity.FindFirst(ClaimTypes.Name).Value;

            }
            var res = _textService.GetTextByEmployeeCode(employeeCode);
            return Ok(res.Data);
        }
        [HttpPut("status")]
        public IActionResult UpdateStatusText([FromBody]Text text) {
            var res = new ServiceResult();
            try {
                var data = _textService.UpdateStatusText(text);
                if (data) {
                    res.Data = null;
                    res.Messenger = BusinessLayer.Properties.Resources.Msg_Success;
                    res.StatusCode = BusinessLayer.Enums.StatusCode.IsValid;
                    return Ok(res);
                }
                else {
                    res.Data = null;
                    res.Messenger = BusinessLayer.Properties.Resources.Msg_Failed;
                    res.StatusCode = BusinessLayer.Enums.StatusCode.NotValid;
                    return BadRequest(res);
                }
            }
            catch (Exception e) {
                res.Data = null;
                res.Messenger = e.Message;
                res.StatusCode = BusinessLayer.Enums.StatusCode.NotValid;
                return BadRequest(res);
            }
            
        }
    }
}
