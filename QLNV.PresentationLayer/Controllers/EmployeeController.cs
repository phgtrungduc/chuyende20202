﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace QLNV.PresentationLayer.Controllers {
    public class EmployeeController : BaseEntityController<Employee> {
        IEmployeeService _employeeService;
        private IWebHostEnvironment env;
        public EmployeeController(IEmployeeService employeeService, IWebHostEnvironment env) : base(employeeService) {
            _employeeService = employeeService;
            this.env = env;
        }
        /// <summary>
        /// Lấy mã nhân viên lớn nhất từ client
        /// </summary>
        /// <returns>mã lớn nhất</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        [HttpGet("maxcode")]
        public IActionResult GetMaxCode() {
            var maxEmployeeCode = _employeeService.GetMaxEmployeeCode();
            return Ok(maxEmployeeCode);
        }
        /// <summary>
        /// FIlter nhân viên theo các tiêu chí
        /// </summary>
        /// <param name="specs">specs có thể là tên,id,code của nhân viên</param>
        /// <param name="DepartmentId">Id phòng ban</param>
        /// <param name="PositionId">id vị trí trong công ty</param>
        /// <returns>Các nhân viên thỏa mãn các tiêu chí trên</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        [HttpGet("filter")]
        public IActionResult GetEmployeeByFilter([FromQuery] string queryString) {
            var maxEmployeeCode = _employeeService.GetEmployeeByFilter(queryString);
            return Ok(maxEmployeeCode);
        }
        [HttpPost("timekeeping")]
        public IActionResult GetTimeKeepingEmployee(IFormFile excelFile) {
            if (excelFile != null) {
                string file_name = Guid.NewGuid().ToString().Replace("-", "") + "_" + excelFile.FileName;
                string extension = Path.GetExtension(file_name);
                if (extension == ".xls" || extension == ".xlsx") {
                    var res = _employeeService.GetTimeKeepingEmployee(excelFile).Result;
                    return Ok(res);
                }
                else {
                    return BadRequest("File upload không phải file Excel");
                }
            }
            else {
                return BadRequest("Chưa chọn file upload");
            }

        }
        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginEntity employee) {
            var res = _employeeService.Login(employee);
            if (res) {
                return Ok();
            }
            else return BadRequest();
        }

        [HttpGet("assignto/{employeeid}")]
        public IActionResult GetTextByAssignTo(Guid employeeId) {
            var res = new ServiceResult();
            IEnumerable<Text> data = null;
            try {
                data = _employeeService.GetTextByAssignTo(employeeId);
                res.Data = data;
                res.Messenger = BusinessLayer.Properties.Resources.Msg_AddSuccess;
                res.StatusCode = BusinessLayer.Enums.StatusCode.IsValid;
                return Ok(res.Data);
            }
            catch (Exception e) {
                res.Data = null;
                res.Messenger = e.Message;
                res.StatusCode = BusinessLayer.Enums.StatusCode.NotValid;
                return BadRequest(res.Data);
            }
        }
        public override IActionResult Post([FromForm] Employee entity) {
            try {
                if (HttpContext.Request.Form.Files != null && HttpContext.Request.Form.Files.Count !=0) {
                    if (HttpContext.Request.Form.Files[0] != null) {
                        if (entity.Avatar == null) {
                            entity.Avatar = CommonFunction.handleImageUpload(HttpContext.Request.Form.Files[0], env.WebRootPath);
                        }
                    }
                }
                return base.Post(entity);
            }
            catch (Exception e) {
                return BadRequest("Lỗi upload ảnh");
                throw;
            }
        }
        public override IActionResult Put(string id, [FromForm] Employee entity) {
            if (HttpContext.Request.Form.Files != null) {
                if (HttpContext.Request.Form.Files.Count != 0) {
                    if (HttpContext.Request.Form.Files[0] != null) {
                        if (entity.Avatar == null) {
                            entity.Avatar = CommonFunction.handleImageUpload(HttpContext.Request.Form.Files[0], env.WebRootPath);
                        }
                    }
                }
                
                
            }
            return base.Put(id,entity);
        }

        [HttpGet("byrole/{role}")]
        public IActionResult GetEmpoyeeByRole(int role) {
            var res = _employeeService.GetEmployeeByRole(role);
            return Ok(res);
        }

        
    }
}
