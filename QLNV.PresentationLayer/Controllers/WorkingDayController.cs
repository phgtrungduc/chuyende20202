﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace QLNV.PresentationLayer.Controllers {
    public class WorkingDayController : BaseEntityController<WorkingDay> {
        IWorkingDayService _workingDayService;
        public WorkingDayController(IWorkingDayService workingDayService) : base(workingDayService) {
            _workingDayService = workingDayService;
        }
        [HttpPost("import")]
        [AllowAnonymous]
        public IActionResult ImportFileWorkingDay(IFormFile excelFile) {
            if (excelFile != null) {
                string file_name = Guid.NewGuid().ToString().Replace("-", "") + "_" + excelFile.FileName;
                string extension = Path.GetExtension(file_name);
                if (extension == ".xls" || extension == ".xlsx") {
                    var res = _workingDayService.ImportFileWorkingDay(excelFile).Result;
                    return Ok("ok");
                    //return Ok(res);
                }
                else {
                    return BadRequest("File upload không phải file Excel");
                }
            }
            else {
                return BadRequest("Chưa chọn file upload");
            }
        }
        
        [HttpGet("bytime")]
        [AllowAnonymous]
        public IActionResult GetWorkingDayByTime([FromQuery] int month,int year,string employeecode) {
            return Ok(_workingDayService.GetWorkingDayByTime(month, year,employeecode));
        }
    }
}
