﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nancy.Json;
using Newtonsoft.Json;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QLNV.PresentationLayer.Controllers {
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class LoginController : ControllerBase {
        ILoginService _loginService;
        IEmployeeService _employeeService;
        public LoginController(ILoginService loginService, IEmployeeService employeeService) {
            _loginService = loginService;
            _employeeService = employeeService;
        }
        // GET: api/<LoginController>
        [HttpGet]
        public IEnumerable<string> Get() {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null) {
                IEnumerable<Claim> claims = identity.Claims;
                // or
                var username = identity.FindFirst(ClaimTypes.Name).Value;

            }
            return new string[] { "value1", "value2" };
        }
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Login([FromBody] LoginEntity entity) {
            //var token = _authenticationManager.Authenticate(username, password);
            var res = _loginService.Login(entity, _employeeService);
            if (res.StatusCode == BusinessLayer.Enums.StatusCode.Success) {
                return Ok(res);
            }
            return BadRequest(res);
        }

        [HttpPost("changepassword")]
        public IActionResult ChangePassword([FromBody] string objectPass) {
            var res = new ServiceResult();
            string username = "";
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null) {
                IEnumerable<Claim> claims = identity.Claims;
                username = identity.FindFirst(ClaimTypes.Name).Value;

            }
            var serializer = new JavaScriptSerializer();
            var desObject = serializer.Deserialize<dynamic>(objectPass);
            var updateRes = _loginService.ChangePassword(username, desObject.oldPass, desObject.newPass, res);
            if (res.StatusCode == BusinessLayer.Enums.StatusCode.Success) {
                return Ok(res);
            }
            else {
                return BadRequest(res);
            }
        }
    }
}
