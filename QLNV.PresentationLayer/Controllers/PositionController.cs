﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;

namespace QLNV.PresentationLayer.Controllers {
    /// <summary>
    /// Controller lấy tất cả các vị trí
    /// </summary>
    /// CreatedBy:PTDuc(04/12/2020)
    public class PositionController : BaseEntityController<Position> {
        IPositionService _positionService;
        public PositionController(IPositionService positionService) : base(positionService) {
            _positionService = positionService;
        }
    }
}
