﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Enums;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace QLNV.PresentationLayer.Controllers {
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class BaseEntityController<TEntity> : ControllerBase {

        IBaseService<TEntity> _baseService;
        public BaseEntityController(IBaseService<TEntity> baseService) {
            _baseService = baseService;
        }

        // GET: api/<CustomersController>
        /// <summary>
        /// Lấy toàn bộ danh sách khách hàng
        /// </summary>
        /// <returns>Danh sách khách hàng</returns>
        /// createdBy:PTDuc(23/12/2020)
        /// đây là ở thằng cha nhá
        [HttpGet]
        public virtual IActionResult Get() {
            var entities = _baseService.GetEntities();
            return Ok(entities);
        }

        // GET api/<CustomersController>/5
        /// <summary>
        /// Tìm kiếm nhân viên theo ID
        /// </summary>
        /// <param name="id">Mã khách hàng</param>
        /// <param name="name"></param>
        /// <returns></returns>
        /// CreatedBy: PTDuc(4/12/2020)
        [HttpGet("{id}")]
        public IActionResult Get(Guid id) {
            var entity = _baseService.GetEntityById(id);
            return Ok(entity);
        }

        // POST api/<CustomersController>
        /// <summary>
        /// Thêm mới 1 đối tượng
        /// </summary>
        /// <param name="entity">Đối tượng truyền lên từ client</param>
        /// <returns></returns>
        /// CreatedBy: PTDuc(4/12/2020)
        [HttpPost]
        public virtual IActionResult Post([FromForm] TEntity entity) {
            try {
                var serviceResult = _baseService.Add(entity);
                if (serviceResult.StatusCode == BusinessLayer.Enums.StatusCode.NotValid) {
                    return BadRequest(serviceResult);
                }
                else {
                    return Ok(serviceResult);
                }
            }
            catch (Exception ex) {
                return BadRequest(ex.Message);
            }

        }

        [HttpPut("{id}")]
        public virtual IActionResult Put([FromRoute] string id, [FromForm] TEntity entity) {
            if (id != null) {
                var keyProperty = entity.GetType().GetProperty($"{typeof(TEntity).Name}Id");
                if (keyProperty.PropertyType == typeof(Guid)) {
                    keyProperty.SetValue(entity, Guid.Parse(id));
                }
                else if (keyProperty.PropertyType == typeof(int)) {
                    keyProperty.SetValue(entity, int.Parse(id));
                }
                else {
                    keyProperty.SetValue(entity, id);
                }
            }
            var res = _baseService.Update(entity);
            if (res.StatusCode == BusinessLayer.Enums.StatusCode.NotValid) {
                return BadRequest(res);
            }
            else {
                return Ok(res);
            }
        }

        // DELETE api/<CustomersController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id) {
            var entity = _baseService.Delete(Guid.Parse(id));
            return Ok(entity);
        }
        public virtual bool preAdd(TEntity entity) {
            return true;
        }
    }
}
