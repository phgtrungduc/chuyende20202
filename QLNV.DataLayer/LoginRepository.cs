﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Data;
using System.Linq;

namespace QLNV.DataLayer {
    public class LoginRepository : BaseRepository<LoginEntity>, ILoginRepository {
        public LoginRepository(IConfiguration configuration) : base(configuration) {
        }

        public LoginEntity Login(string Username) {
            LoginEntity entity = _dbConnection.Query<LoginEntity>($"Proc_GetLogin", new { @Username=Username }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            return entity;
        }

        public int UpdatePasswordByEmployeeCode(string employeecode, string password,string salt) {
            var rows = 0;
            rows = _dbConnection.Execute("Proc_UpdatePasswordByEmployeeCode", new {EmployeeCode = employeecode,Password = password,Salt = salt },commandType:CommandType.StoredProcedure);
            return rows;
        }
    }
}
