﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QLNV.DataLayer {
    public class WorkingDayRepository : BaseRepository<WorkingDay>, IWorkingDayRepository {
        public WorkingDayRepository(IConfiguration configuration) : base(configuration) {
        }

        public IEnumerable<WorkingDay> GetWorkingDayByTime(int month, int year, string employeecode) {
            var list = _dbConnection.Query<WorkingDay>("Proc_GetWorkingDayByTime",new {Month = month,Year = year,EmployeeCode = employeecode },commandType: CommandType.StoredProcedure);
            return list;
        }
    }
}
