﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QLNV.DataLayer {
    public class TextRepository : BaseRepository<Text>, ITextRepository {
        public TextRepository(IConfiguration configuration) : base(configuration) {
        }

        public IEnumerable<Text> GetTextByEmployeeCode(string code) {
            var res = _dbConnection.Query<Text>($"Proc_GetTextByEmployeeCode", new {EmployeeCode = code },commandType: CommandType.StoredProcedure);
            return res;
        }

        public bool UpdateStatusText(Text text) {
            var res = false;
            var dynamicParam = CommonFunction.MappingDBType(text);
            var rows = _dbConnection.Execute($"Proc_UpdateText", dynamicParam, commandType: CommandType.StoredProcedure);
            if (rows != 0) res= true;
            return res;
        }
    }
}
