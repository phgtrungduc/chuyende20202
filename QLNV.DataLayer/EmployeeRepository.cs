﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace QLNV.DataLayer {
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository {

        public EmployeeRepository(IConfiguration configuration) : base(configuration) {
        }

        //public override int Add(Employee employee) {
        //    var res = 0;
        //    var password = employee.Password;
        //    var passObject = CommonFunction.hashPassWord(password);
        //    var hashPass = passObject.GetType().GetProperty("Password").GetValue(passObject);
        //    var salt = passObject.GetType().GetProperty("Salt").GetValue(passObject).ToString();
        //    employee.Password = hashPass.ToString();
        //    _dbConnection.Open();
        //    var newId = Guid.NewGuid();
        //    employee.EmployeeId = newId;
        //    using (var transaction = _dbConnection.BeginTransaction()) {
        //        try {
        //            DynamicParameters parameters = CommonFunction.MappingDBType(employee);
        //            //Thực thi 
        //            res +=  _dbConnection.Execute("Proc_InsertLogin", new { EmployeeId = employee.EmployeeId.ToString(), Password = hashPass, Salt = salt,CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now } , commandType: CommandType.StoredProcedure, transaction: transaction );
        //            res += _dbConnection.Execute($"Proc_Insert{_tableName}", parameters, commandType: CommandType.StoredProcedure, transaction: transaction);

        //            //Trả lại số bản
        //            transaction.Commit();
        //        }
        //        catch (Exception e) {
        //            System.Diagnostics.Debug.Write(e.Message);
        //            transaction.Rollback();
        //        }
        //    }
        //    return res;
        //}
        public override void PreAdd(Employee entity) {
            var password = entity.Password;
            if (password!=null) {
                var passObject = CommonFunction.hashPassWord(password);
                var Hashpass = passObject.GetType().GetProperty("Password").GetValue(passObject);
                var Salt = passObject.GetType().GetProperty("Salt").GetValue(passObject);
                entity.Password = Hashpass.ToString();
                entity.Salt = Salt.ToString();
            }
        }

        /// <summary>
        /// Tìm kiếm nhân viên theo mã nhân viên
        /// </summary>
        /// <param name="employeeCode">mã nhân viên</param>
        /// <returns>List các nhân viên có mã thỏa mãn</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public Employee GetEmployeeByCode(string employeeCode) {
            var employee = _dbConnection.Query<Employee>($"Select * from {_tableName} where EmployeeCode='{employeeCode}'").FirstOrDefault();
            return employee;
        }


        /// <summary>
        /// Tìm kiếm nhân viên theo các tiêu chí
        /// </summary>
        /// <param name="specs">string xét theo id hoặc tên hoặc mã nhân viên</param>
        /// <param name="DepartmentId">id phòng ban</param>
        /// <param name="PositionId">id chức vụ</param>
        /// <returns>các nhân viên thõa mãn các tiêu chí</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public IEnumerable<Employee> GetEmployeeByFilter(string queryString) {
            var employee = _dbConnection.Query<Employee>("Proc_GetEmployeeByFilter",new {query = queryString },commandType:CommandType.StoredProcedure);
            return employee;
        }

        /// <summary>
        /// Lấy ra mã khách hàng lớn nhất trên hệ thông
        /// </summary>
        /// <returns>mã lớn nhất</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public string GetMaxEmployeeCode() {
            var maxEmployeeCode = _dbConnection.Query<string>("SELECT MAX(EmployeeCode) FROM Employee").FirstOrDefault();
            return maxEmployeeCode.ToString();
        }
        
        /// <summary>
        /// Lấy ra mã khách hàng lớn nhất trên hệ thông
        /// </summary>
        /// <returns>mã lớn nhất</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public LoginEntity Login(LoginEntity employee) {
            //var query = String.Format("SELECT EmployeeCode as Username,Password as HashPass, Salt FROM Employee,Login WHERE Employee.EmployeeId=Login.EmployeeId AND EmployeeCode = '{0}'", employee.Username);
            //var loginInfor = this.ExcuteReader("Proc_InsertTest", );
            return null ;
        }

        public IEnumerable<Text> GetTextByAssignTo(Guid assignTo) {
            var text = _dbConnection.Query<Text>("Proc_GetTextByAssignTo",new { AssignTo = assignTo.ToString()},commandType:CommandType.StoredProcedure);
            return text;
        }

        public IEnumerable<Employee> GetEmployeeByRole(int role) {
            return _dbConnection.Query<Employee>("Select * From Employee where Role = "+role, commandType: CommandType.Text);

        }
    }
}
