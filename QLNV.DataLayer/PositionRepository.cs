﻿using Microsoft.Extensions.Configuration;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.DataLayer {
    public class PositionRepository : BaseRepository<Position>, IPositionRepository {
        public PositionRepository(IConfiguration configuration) : base(configuration) {
        }

    }
}
