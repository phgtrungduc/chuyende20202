﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Enums;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace QLNV.DataLayer {
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>, IDisposable where TEntity : BaseEntity {
        protected string _tableName;
        #region DECLARE
        IConfiguration _configuration;
        String _connectionString = null;
        protected IDbConnection _dbConnection = null;
        protected SqlDatabase sqlDataBase = null;
        #endregion
        public BaseRepository(IConfiguration configuration) {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("MISACukCukConnectionString");
            _dbConnection = new SqlConnection(_connectionString);
            sqlDataBase = new SqlDatabase(_connectionString);
            _tableName = typeof(TEntity).Name;
        }

        public int ExcuteReader(string storeName, object param) {
            int rows = 0;
            DynamicParameters parameters = null;
            //if (param != null) parameters = CommonFunction.MappingDBType();
            rows = _dbConnection.Execute(storeName, parameters, commandType: CommandType.StoredProcedure);
            return rows;
        }

        /// <summary>
        /// Thêm đối tượng
        /// </summary>
        /// <param name="entity">Đối tượng truyền lên từ client</param>
        /// <returns>Số lượng bản ghi bị ảnh hưởng</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public int Add(TEntity entity, ServiceResult result) {
            entity.EntityState = EntityState.AddNew;
            this.PreAdd(entity);
            var rows = 0;
            _dbConnection.Open();
            using (var transaction = _dbConnection.BeginTransaction()) {
                try {
                    //var command = sqlDataBase.GetStoredProcCommand($"Proc_Insert{_tableName}");
                    //sqlDataBase.DiscoverParameters(command);
                    //DynamicParameters parameters = new DynamicParameters();
                    //foreach (SqlParameter param in command.Parameters) {
                    //    var name = param.ParameterName;
                    //    if (name != "@RETURN_VALUE") {
                    //        var nameProp = name.Replace("@","");
                    //        var value = entity.GetType().GetProperty(nameProp).GetValue(entity);
                    //        if (nameProp == "CreatedDate"|| nameProp == "ModifiedDate") {
                    //            parameters.Add(nameProp, DateTime.Now);
                    //        }
                    //        else if (nameProp == _tableName + "Id") {
                    //            parameters.Add(nameProp, Guid.NewGuid().ToString());
                    //        }
                    //        else {
                    //            parameters.Add(nameProp, value);
                    //        }
                    //    }
                    //}

                    var parameters = this.BuildDBParam(entity, $"Proc_Insert{_tableName}");


                    //Thực thi 
                    rows = _dbConnection.Execute($"Proc_Insert{_tableName}", parameters, commandType: CommandType.StoredProcedure, transaction: transaction);
                    //Trả lại số bản
                    transaction.Commit();
                }
                catch (Exception e) {
                    result.Messenger += e.Message;
                    transaction.Rollback();
                }
                finally {
                    _dbConnection.Close();
                }
            }
            return rows;
        }

        /// <summary>
        /// Xóa đối tượng theo Id
        /// </summary>
        /// <param name="entityId">Id của đối tượng</param>
        /// <returns>số lượng bản ghi bị ảnh hưởng</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public int Delete(Guid entityId) {
            var rows = 0;
            _dbConnection.Open();
            using (var transaction = _dbConnection.BeginTransaction()) {
                try {
                    rows = _dbConnection.Execute($"DELETE FROM {_tableName} WHERE {_tableName}Id='{entityId}'", entityId, commandType: CommandType.Text,transaction:transaction);
                    transaction.Commit();
                }
                catch (Exception) {
                    transaction.Rollback();
                }
            }

            return rows;
        }


        /// <summary>
        /// Lấy tất cả các đối tượng
        /// </summary>
        /// <returns>List các đối tượng</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public IEnumerable<TEntity> GetEntities() {
            var entities = _dbConnection.Query<TEntity>($"Proc_Get{_tableName}", commandType: CommandType.StoredProcedure);//Chạy câu lệnh đầu query

            //Trả về dữ liệu
            return entities;
        }

        /// <summary>
        /// TÌm kiếm đối tượng theo id
        /// </summary>
        /// <param name="entityId">id của đối tượng</param>
        /// <returns>List các đối tượng có id</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public TEntity GetEntityById(Guid entityId) {
            //Khởi tạo các commandtext, trả về thằng đầu tiên nếu có, nếu không trả về null
            var entities = _dbConnection.Query<TEntity>($"SELECT * FROM {_tableName} WHERE {_tableName}Id='{entityId}'", commandType: CommandType.Text).FirstOrDefault();//Chạy câu lệnh đầu query
            //Trả về dữ liệu
            return entities;
        }
        /// <summary>
        /// Update thông tin đối tượng
        /// </summary>
        /// <param name="entity">đối tượng với các thông tin cần update</param>
        /// <returns>số lượng bản ghi bị ảnh hưởng </returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public int Update(TEntity entity,ServiceResult serviceResult) {
            entity.EntityState = EntityState.Update;
            var rowAffects = 0;
            _dbConnection.Open();
            using (var transaction = _dbConnection.BeginTransaction()) {
                try {
                    var parameters = BuildDBParam(entity, $"Proc_Update{_tableName}");
                    // Thực thi commandText:
                    rowAffects = _dbConnection.Execute($"Proc_Update{_tableName}", parameters, commandType: CommandType.StoredProcedure,transaction:transaction);
                    transaction.Commit();
                }
                catch (Exception e) {
                    serviceResult.Messenger += e.Message;
                    serviceResult.StatusCode = StatusCode.NotValid;
                    transaction.Rollback();
                }
            }

            // Trả về kết quả (số bản ghi thêm mới được)
            return rowAffects;
        }

        /// <summary>
        /// Mapping dữ liệu để thêm vào database
        /// </summary>
        /// <param name="entity">đối tượng cần mapping</param>
        /// <returns>đối tượng sau khi đã mapping</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        private DynamicParameters MappingDBType(TEntity entity) {
            var parameters = new DynamicParameters();
            var properties = entity.GetType().GetProperties();
            //Mapping dữ liệu từ C# sang mariadb
            foreach (var property in properties) {
                var propertyName = property.Name;
                var propertyType = property.PropertyType;
                var propertyValue = property.GetValue(entity);
                if (propertyName == "EntityState") {
                    continue;
                }
                if (propertyName == "CreatedDate" && entity.EntityState == EntityState.AddNew) {
                    parameters.Add("@CreatedDate", new DateTime());
                }
                if (propertyName == "ModifiedDate" && entity.EntityState == EntityState.Update) {
                    parameters.Add("@ModifiedDate", new DateTime());
                }
                if (propertyType.Name == "Guid" || propertyType.Name == "Guid?") {
                    parameters.Add($"@{propertyName}", propertyValue, DbType.String);
                }
                else {
                    parameters.Add($"@{propertyName}", propertyValue);
                }
            }
            return parameters;
        }
        /// <summary>
        /// Hàm này để dùng chung cho việc tìm kiếm theo 1 tiêu chí nào đó
        /// </summary>
        /// <param name="entity">pbject có thuộc tính cần kiểm tra, gửi thông tin check trong db</param>
        /// <param name="property">thuộc tính cần kiểm tra xem đã tồn tại trong db hay chưa</param>
        /// <returns>trả lại kết quả đầu tiên tìm thấy, nếu không trả lại null</returns>
        public TEntity GetEntityByProperty(TEntity entity, PropertyInfo property) {
            //Tên thuộc tính cần kiểm tra
            var propertyName = property.Name;
            //giá trị thuộc tính cần kiểm tra
            var propertyValue = property.GetValue(entity);
            //lấy giá trị thuộc tính khóa chính (customerID) để phục vụ cho việc query khi update(tìm xem có bản ghi trùng trừ bản ghi đã truyền vào chính là entity)
            var keyValue = entity.GetType().GetProperty($"{_tableName}Id").GetValue(entity);
            var query = string.Empty;
            if (entity.EntityState == EntityState.AddNew) {
                //Nếu là phương thức thêm thì chỉ cần kiểm tra xem đã tồn tại hay chưa 
                query = $"SELECT * FROM {_tableName} WHERE {propertyName} = '{propertyValue}'";
            }

            else if (entity.EntityState == EntityState.Update) {
                query = $"SELECT * FROM {_tableName} WHERE {propertyName} = '{propertyValue}' AND {_tableName}Id <> '{keyValue}'";
            }
            else
                return null;
            var entityReturn = _dbConnection.Query<TEntity>(query, commandType: CommandType.Text).FirstOrDefault();
            return entityReturn;
        }
        /// <summary>
        /// Lấy ra mã khách hàng lớn nhất trên hệ thông
        /// </summary>
        /// <returns>mã lớn nhất</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public AutoIdEntity GetAutoId() {
            var maxEmployeeCode = _dbConnection.Query<AutoIdEntity>("Proc_GetAutoId", new { TableName = _tableName }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            return maxEmployeeCode;
        }
        public virtual void PreAdd(TEntity entity) {

        }

        protected DynamicParameters BuildDBParam(TEntity entity, string store) {
            var command = sqlDataBase.GetStoredProcCommand(store);
            sqlDataBase.DiscoverParameters(command);
            DynamicParameters parameters = new DynamicParameters();
            foreach (SqlParameter param in command.Parameters) {
                var name = param.ParameterName;
                if (name != "@RETURN_VALUE") {
                    var nameProp = name.Replace("@", "");
                    var value = entity.GetType().GetProperty(nameProp).GetValue(entity);
                    if (nameProp == "CreatedDate" && entity.EntityState == EntityState.AddNew) {
                        parameters.Add(nameProp, DateTime.Now);
                    }
                    else if (nameProp == "ModifiedDate") {
                        parameters.Add(nameProp, DateTime.Now);
                    }
                    else if (nameProp == _tableName + "Id") {
                        if (entity.EntityState == EntityState.AddNew) {

                            parameters.Add(nameProp, Guid.NewGuid().ToString());
                        }
                        else if (entity.EntityState == EntityState.Update) {
                            parameters.Add(nameProp, value.ToString());
                        };
                    }
                    else {
                        parameters.Add(nameProp, value);
                    }
                }
            }
            return parameters;
        }


        public void Dispose() {
            if (_dbConnection.State == ConnectionState.Open) {
                _dbConnection.Close();
            }
        }
    }
}
