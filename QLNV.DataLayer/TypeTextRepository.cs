﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QLNV.DataLayer {
    public class TypeTextRepository : BaseRepository<TypeText>, ITypeTextRepository {
        public TypeTextRepository(IConfiguration configuration) : base(configuration) {
        }
    }
}
