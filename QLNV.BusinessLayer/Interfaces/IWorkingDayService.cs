﻿using Microsoft.AspNetCore.Http;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QLNV.BusinessLayer.Interfaces {
    public interface IWorkingDayService : IBaseService<WorkingDay> {
        Task<List<WorkingDay>> ImportFileWorkingDay(IFormFile excelFile);
        ServiceResult GetWorkingDayByTime(int month, int year,string employeecode);
    }
}