﻿using Microsoft.AspNetCore.Http;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QLNV.BusinessLayer.Interfaces {
    public interface IEmployeeService : IBaseService<Employee> {
        string GetMaxEmployeeCode();
        IEnumerable<Employee> GetEmployeeByFilter(string queryString);
        Task<List<WorkingDay>> GetTimeKeepingEmployee(IFormFile excelFile);
        bool Login(LoginEntity employee);
        IEnumerable<Text> GetTextByAssignTo(Guid assignTo);
        Employee GetEmployeeByCode(string employeeCode);
        IEnumerable<Employee> GetEmployeeByRole(int role);
        bool ChangePassword(LoginEntity employee);
    }
}
