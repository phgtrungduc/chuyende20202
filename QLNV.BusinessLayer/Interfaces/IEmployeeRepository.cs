﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace QLNV.BusinessLayer.Interfaces {
    public interface IEmployeeRepository : IBaseRepository<Employee> {
        /// <summary>
        /// Lấy thông tin nhân viên theo mã
        /// </summary>
        /// <param name="">mã nhân viên</param>
        /// <returns></returns>
        Employee GetEmployeeByCode(string employeeCode);
        string GetMaxEmployeeCode();
        IEnumerable<Employee> GetEmployeeByFilter(string queryString);
        LoginEntity Login(LoginEntity employee);
        IEnumerable<Text> GetTextByAssignTo(Guid assignTo);
        IEnumerable<Employee> GetEmployeeByRole(int role);
    }
}
