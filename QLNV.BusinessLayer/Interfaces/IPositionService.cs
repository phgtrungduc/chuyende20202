﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Interfaces {
    public interface IPositionService: IBaseService<Position> {
    }
}
