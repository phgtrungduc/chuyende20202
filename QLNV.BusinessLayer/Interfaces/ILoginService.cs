﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections;
using System.Collections.Generic;

namespace QLNV.BusinessLayer.Interfaces {
    public interface ILoginService {
        ServiceResult Login(LoginEntity entity, IEmployeeService employeeService);
        bool ChangePassword(string username,string oldPassword,string newPassword,ServiceResult serviceResult);
    }
}