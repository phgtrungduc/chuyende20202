﻿using QLNV.BusinessLayer.Entity;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace QLNV.BusinessLayer.interfaces {
    public interface IBaseRepository<TEntity> {
        IEnumerable<TEntity> GetEntities();
        TEntity GetEntityById(Guid entityId);
        int ExcuteReader(string storeName, object param);
        int Add(TEntity entity,ServiceResult result);
        int Update(TEntity entity, ServiceResult result);
        int Delete(Guid entityId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity">object thực thể, ví dụ customer,entity</param>
        /// <param name="property">thuộc tính cần kiểm tra</param>
        /// <returns></returns>
        TEntity GetEntityByProperty(TEntity entity,PropertyInfo property);
        AutoIdEntity GetAutoId();
        void PreAdd(TEntity entity);
    }
}
