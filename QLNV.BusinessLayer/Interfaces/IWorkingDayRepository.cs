﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Interfaces {
    public interface IWorkingDayRepository: IBaseRepository<WorkingDay> {
        IEnumerable<WorkingDay> GetWorkingDayByTime(int month,int year, string employeecode);
    }
}
