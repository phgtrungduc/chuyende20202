﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System.Collections.Generic;

namespace QLNV.BusinessLayer.Interfaces {
    public interface ITypeTextRepository : IBaseRepository<TypeText> {
    }
}
