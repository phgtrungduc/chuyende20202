﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;

namespace QLNV.BusinessLayer.Interfaces {
    public interface ILoginRepository: IBaseRepository<LoginEntity> {
        LoginEntity Login(string Username);
        int UpdatePasswordByEmployeeCode(string employeecode,string password, string salt);
    }
}
