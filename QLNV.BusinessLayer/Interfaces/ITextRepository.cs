﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System.Collections.Generic;

namespace QLNV.BusinessLayer.Interfaces {
    public interface ITextRepository : IBaseRepository<Text> {
        public IEnumerable<Text> GetTextByEmployeeCode(string code);
        public bool UpdateStatusText(Text text);
    }
}
