﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections;
using System.Collections.Generic;

namespace QLNV.BusinessLayer.Interfaces {
    public interface ITextService:IBaseService<Text>{
        public ServiceResult GetTextByEmployeeCode(string code);
        public bool UpdateStatusText(Text text);
    }
}