﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Interfaces {
    public interface IAuthenticationManager {
        public Object Authenticate(string username,string password);
    }
}
