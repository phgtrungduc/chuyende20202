﻿using Dapper;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Enums;
using System;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QLNV.BusinessLayer {
    public class CommonFunction {
        public const int ImageMinimumBytes = 512;
        public static object hashPassWord(string password) {

            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create()) {
                rng.GetBytes(salt);
            }
            var dbSalt = Convert.ToBase64String(salt);
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            return new { Password = hashed, Salt = dbSalt };
        }
        public static bool validatePassword(string password, string hashPass, string saltPass) {
            var salt = Convert.FromBase64String(saltPass);
            string hashedPass = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            return hashedPass == hashPass;
        }
        public static DynamicParameters MappingDBType(BaseEntity entity) {
            var parameters = new DynamicParameters();
            var properties = entity.GetType().GetProperties();
            //Mapping dữ liệu từ C# sang mariadb

            var entityState = entity.EntityState;
            if (entityState == EntityState.AddNew) {
                parameters.Add("@CreatedDate", DateTime.Now);
                parameters.Add("@ModifiedDate", DateTime.Now);
            }
            if (entityState == EntityState.Update) {
                parameters.Add("@ModifiedDate", DateTime.Now);
            }
            foreach (var property in properties) {
                    var propertyName = property.Name;
                    var propertyType = property.PropertyType;
                    var propertyValue = property.GetValue(entity);
                    if (propertyName == "EntityState" || propertyName == "ModifiedDate" || propertyName == "CreatedDate") {
                        continue;
                    }

                    if (propertyType.Name == "Guid" || propertyType.Name == "Guid?") {
                        if ((Guid)propertyValue == Guid.Empty) {
                            parameters.Add($"@{propertyName}", Guid.NewGuid().ToString());
                        }
                        else parameters.Add($"@{propertyName}", propertyValue.ToString());
                    }
                    else {
                        parameters.Add($"@{propertyName}", propertyValue);
                    }
                    
            }
            return parameters;
        }
        public static string handleImageUpload(IFormFile image, string serverPath) {
            string res=null ;
            if (isImage(image)) {
                var nameFieldFile = image.Name;
                var fileName = image.FileName;
                var uploadDirectory = "uploads\\" + nameFieldFile.ToLower();
                var uploadPath = Path.Combine(serverPath, uploadDirectory);
                if (!Directory.Exists(uploadPath)) Directory.CreateDirectory(uploadPath);
                var extension = image.ContentType;
                var strFilePath = Guid.NewGuid().ToString().Replace("-","") +"_"+ fileName;
                var des  = Path.Combine(uploadPath, strFilePath);
                using (var stream = new FileStream(des, FileMode.Create)) {
                    image.CopyTo(stream);
                }
                res = Path.Combine(uploadDirectory ,strFilePath);
            }

            return res;
        }
        public static bool isImage(IFormFile file) {
            //-------------------------------------------
            //  Check the image mime types
            //-------------------------------------------
            if (file.ContentType.ToLower() != "image/jpg" &&
                        file.ContentType.ToLower() != "image/jpeg" &&
                        file.ContentType.ToLower() != "image/pjpeg" &&
                        file.ContentType.ToLower() != "image/gif" &&
                        file.ContentType.ToLower() != "image/x-png" &&
                        file.ContentType.ToLower() != "image/png") {
                return false;
            }

            //-------------------------------------------
            //  Check the image extension
            //-------------------------------------------
            if (Path.GetExtension(file.FileName).ToLower() != ".jpg"
                && Path.GetExtension(file.FileName).ToLower() != ".png"
                && Path.GetExtension(file.FileName).ToLower() != ".gif"
                && Path.GetExtension(file.FileName).ToLower() != ".jpeg") {
                return false;
            }

            //-------------------------------------------
            //  Attempt to read the file and check the first bytes
            //-------------------------------------------
            try {
                if (!file.OpenReadStream().CanRead) {
                    return false;
                }
                //------------------------------------------
                //check whether the image size exceeding the limit or not
                //------------------------------------------ 
                if (file.Length < ImageMinimumBytes) {
                    return false;
                }

                byte[] buffer = new byte[ImageMinimumBytes];
                file.OpenReadStream().Read(buffer, 0, ImageMinimumBytes);
                string content = System.Text.Encoding.UTF8.GetString(buffer);
                if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
                    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline)) {
                    return false;
                }
            }
            catch (Exception) {
                return false;
            }

            //-------------------------------------------
            //  Try to instantiate new Bitmap, if .NET will throw exception
            //  we can assume that it's not a valid image
            //-------------------------------------------

            try {
                using (var bitmap = new Bitmap(file.OpenReadStream())) {
                }
            }
            catch (Exception) {
                return false;
            }
            finally {
                file.OpenReadStream().Position = 0;
            }
            return true;
        }
    }
}