﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Entity {
    public class Text:BaseEntity {
        public Guid TextId { get; set; }
        public string EmployeeCode { get; set; }
        public int TypeText { get; set; }
        public string Content { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ToDate { get; set; }
        public Guid AssignTo{ get; set; }
        //0-chưa duyệt 1-chấp nhận 2-từ chối
        public int Status { get; set; }
        public string EmployeeFullName { get; set; }
        public string AssignToFullName { get; set; }
    }
}
