﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace QLNV.BusinessLayer.Entity {
    public class Employee : BaseEntity {
        #region Declare
        #endregion

        #region Constructor
        public Employee() { }
        #endregion

        #region Properties
        /// <summary>
        /// Id Nhân viên
        /// </summary>
        public Guid EmployeeId { get; set; }
        /// <summary>
        /// Mã nhân viên
        /// </summary>
        [CheckDuplicate]
        [Required]
        [DisplayName("mã nhân viên")]
        public string EmployeeCode { get; set; }
        /// <summary>
        /// Họ và tên khách hàng
        /// </summary>
        
        [Required]
        [DisplayName("họ và tên")]
        public string FullName { get; set; }
        /// <summary>
        /// Ngày - tháng - năm sinh
        /// </summary>
        
        public DateTime? DateOfBirth { get; set; }
        /// <summary>
        /// Giới tính (0 - Nữ; 1 - Nam; 2 - Khác)
        /// </summary>
        
        public int? Gender { get; set; }
        /// <summary>
        /// Email khách hàng
        /// </summary>
        
        [Email]
        [CheckDuplicate]
        [DisplayName("email")]
        public string Email { get; set; }
        /// <summary>
        /// Số điện thoại khách hàng
        /// </summary
        
        [CheckDuplicate]
        [Required]
        [DisplayName("số điện thoại")]
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Số chứng minh nhân dân
        /// </summary>
        
        [DisplayName("số chứng minh thư")]
        public string IdentityNumber { get; set; }
        /// <summary>
        /// Ngày cấp chứng minh nhân dân
        /// </summary>
        
        public DateTime? IdentityDate { get; set; }
        /// <summary>
        /// Nơi cấp chứng minh nhân
        /// </summary>
        
        public string IdentityPlace { get; set; }
        /// <summary>
        /// Ngày gia nhập công ty
        /// </summary>
        
        public DateTime? JoinDate { get; set; }
        /// <summary>
        /// Mã số thuế cá nhân
        /// </summary>
        
        public string PersonalTaxCode { get; set; }
        /// <summary>
        /// Mức lương cơ bản
        /// </summary>
        
        public double? Salary { get; set; }
        /// <summary>
        /// Tình trạng làm việc (0:Đang làm việc;1-Đang thử việc;2-Đã nghỉ việc)
        /// </summary>
        
        public int? WorkStatus { get; set; }
        // <summary>
        /// avatar
        /// </summary>
        
        public string Avatar { get; set; }
        // <summary>
        /// password
        /// </summary>
        [Required]
        
        public string Password { get; set; }
        /// <summary>
        /// Biến muối
        /// </summary>
        
        public string Salt { get; set; }
        /// <summary>
        /// vai trò trên hệ thống (0-nhân viên,2-HR,3-giám đốc các bộ phận)
        /// </summary>
        [Required]
        
        public int Role { get; set; }

        #endregion
        #region Method
        #endregion
    }
}
