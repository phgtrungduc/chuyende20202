﻿using System.Collections;
using System.ComponentModel;

namespace QLNV.BusinessLayer.Entity {
    public class LoginEntity : BaseEntity {
        [Required]
        [DisplayName("Tên đăng nhập")]
        public string Username { get; set; }
        [Required]
        [DisplayName("Mật khẩu")]
        public string Password { get; set; }
        public string Salt { get; set; }

    }
}