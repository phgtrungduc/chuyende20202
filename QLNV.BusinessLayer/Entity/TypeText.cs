﻿namespace QLNV.BusinessLayer.Entity {
    public class TypeText : BaseEntity {
        public int TypeTextCode { get; set; }
        public string TypeTextName { get; set; }
    }
}