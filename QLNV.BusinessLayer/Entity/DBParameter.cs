﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Entity {
    public class DBParameter {
        public string paramName{ get; set; }
        public object paramValue { get; set; }
    }
}
