﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Entity {
    public class AutoIdEntity: BaseEntity{
        public string TableName { get; set; }
        public string Prefix { get; set; }
        public int Id { get; set; }
    }
}
