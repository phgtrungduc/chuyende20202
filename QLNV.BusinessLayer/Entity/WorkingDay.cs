﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Entity {
    public class WorkingDay:BaseEntity {
        public Guid WorkingDayId { get; set; }
        public string EmployeeCode { get; set; }
        public string FullName { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string DetailWorkingDay { get; set; }

        public WorkingDay() { }

        public WorkingDay(Guid workingDayId,string employeeCode, int month, int year, string detailWorkingDay) {
            WorkingDayId = workingDayId;
            EmployeeCode = employeeCode;
            Month = month;
            Year = year;
            DetailWorkingDay = detailWorkingDay;
        }
    }
}
