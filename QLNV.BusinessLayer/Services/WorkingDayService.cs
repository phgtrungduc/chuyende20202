﻿using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace QLNV.BusinessLayer.Services {
    public class WorkingDayService: BaseService<WorkingDay>, IWorkingDayService {
        IHostingEnvironment _appEnvironment;
        IWorkingDayRepository _workingDayRepository;
        public WorkingDayService(IWorkingDayRepository workingDayRepository, IHostingEnvironment appEnvironment) : base(workingDayRepository) {
            _workingDayRepository = workingDayRepository;
            _appEnvironment = appEnvironment;
        }

        public ServiceResult GetWorkingDayByTime(int month, int year, string employeecode) {
            var res = new ServiceResult();
            res.Messenger = Properties.Resources.Msg_Success;
            res.Data = _workingDayRepository.GetWorkingDayByTime(month, year, employeecode);
            return res;
        }
        public async Task<List<WorkingDay>> ImportFileWorkingDay(IFormFile excelFile) {
            string file_name = Guid.NewGuid().ToString().Replace("-", "") + "_" + excelFile.FileName;
            string uploads = Path.Combine(_appEnvironment.WebRootPath, "uploads","workingday");
            string urlPart = Path.Combine(uploads,file_name);
            List<WorkingDay> workingDay = new List<WorkingDay>();
            if (!Directory.Exists(uploads)) Directory.CreateDirectory(uploads);
            using (var fileStream = new FileStream(Path.Combine(uploads, file_name), FileMode.Create)) {
                await excelFile.CopyToAsync(fileStream);
            }
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var month = DateTime.Now.Date.ToString("MM");
            var year = DateTime.Now.Date.ToString("yyyy"); ;
            using (var stream = File.Open(urlPart, FileMode.Open, FileAccess.Read)) {
                using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                    do {
                        if (reader.Name == "Sheet1" || reader.Name == "Sheet2") {
                            var isFirstRow = true;
                            while (reader.Read()&&reader.GetValue(0)!=null) {
                                if (isFirstRow) {
                                    isFirstRow = false;
                                    continue;
                                }
                                else {
                                    var EmployeeCode = reader.GetValue(0).ToString();
                                    var DetailWorkingDay = new List<int>();
                                    for (int i = 0; i < 31; i++) {
                                        DetailWorkingDay.Add(int.Parse(reader.GetValue(i + 1).ToString()));
                                    }
                                    var detailWorkingDayEmployee = string.Join(",", DetailWorkingDay.ToArray()); ;
                                    var workingDayEmployee = new WorkingDay(Guid.NewGuid(), EmployeeCode, int.Parse(month), int.Parse(year), detailWorkingDayEmployee);
                                    _workingDayRepository.Add(workingDayEmployee,_serviceResult);
                                }
                            }
                        }
                    } while (reader.NextResult());
                    //if (employee.Count > 0) {
                    //    return employee;
                    //}
                    //else {
                    //    return null;
                    //}
                }
            }
            return null;
        }
    }
}
