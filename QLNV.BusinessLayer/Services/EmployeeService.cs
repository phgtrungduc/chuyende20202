﻿using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace QLNV.BusinessLayer.Services {
    public class EmployeeService : BaseService<Employee>, IEmployeeService {
        IHostingEnvironment _appEnvironment;
        IEmployeeRepository _employeeRepository;
        #region Constructor
        public EmployeeService(IEmployeeRepository employeeRepository, IHostingEnvironment appEnvironment) : base(employeeRepository) {
            _employeeRepository = employeeRepository;
            this._appEnvironment = appEnvironment;
        }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public override int AfterAdd() {
           var rows =  _employeeRepository.ExcuteReader("Proc_UpdateAutoId", new {TableName = "Employee" });
            return rows;
        }

        public Employee GetEmployeeByCode(string employeeCode) {
            return _employeeRepository.GetEmployeeByCode(employeeCode);
        }

        /// <summary>
        /// FIlter nhân viên theo các tiêu chí
        /// </summary>
        /// <param name="specs">specs có thể là tên,id,code của nhân viên</param>
        /// <param name="DepartmentId">Id phòng ban</param>
        /// <param name="PositionId">id vị trí trong công ty</param>
        /// <returns>Các nhân viên thỏa mãn các tiêu chí trên</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public IEnumerable<Employee> GetEmployeeByFilter(string queryString) {
            return _employeeRepository.GetEmployeeByFilter(queryString);
        }

        public IEnumerable<Employee> GetEmployeeByRole(int role) {
            return _employeeRepository.GetEmployeeByRole(role);
        }

        /// <summary>
        /// Lấy mã nhân viên lớn nhất từ client
        /// </summary>
        /// <returns>mã lớn nhất</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public string GetMaxEmployeeCode() {
            var object1 = _employeeRepository.GetAutoId();
            return _employeeRepository.GetMaxEmployeeCode();
        }

        public IEnumerable<Text> GetTextByAssignTo(Guid assignTo) {
            return _employeeRepository.GetTextByAssignTo(assignTo);
        }

        public async Task<List<WorkingDay>> GetTimeKeepingEmployee(IFormFile excelFile) {
            string file_name = Guid.NewGuid().ToString().Replace("-", "") + "_" + excelFile.FileName;
            string uploads = Path.Combine(_appEnvironment.WebRootPath, "uploads");
            string urlPart = uploads + "/" + file_name;
            List<WorkingDay> employee = new List<WorkingDay>();
            using (var fileStream = new FileStream(Path.Combine(uploads, file_name), FileMode.Create)) {
                await excelFile.CopyToAsync(fileStream);
            }
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            using (var stream = File.Open(urlPart, FileMode.Open, FileAccess.Read)) {
                using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                    do {
                        if (reader.Name == "Sheet1" || reader.Name == "Sheet2") {
                            while (reader.Read()) {
                                //employee.Add(new WorkingDay {
                                //    FullName = reader.GetValue(0).ToString(),
                                //    NumberOfDay = int.Parse(reader.GetValue(1).ToString())
                                //});
                            }
                        }
                    } while (reader.NextResult());
                    if (employee.Count > 0) {
                        return employee;
                    }
                    else {
                        return null;
                    }
                }
            }
        }

        public bool Login(LoginEntity employee) {
            var employeeInfor = _employeeRepository.Login(employee);
            bool valid = CommonFunction.validatePassword(employee.Password, employeeInfor.Password, employeeInfor.Salt);
            return valid;
        }
        public bool ChangePassword(LoginEntity employee) {
            var employeeInfor = _employeeRepository.Login(employee);
            bool valid = CommonFunction.validatePassword(employee.Password, employeeInfor.Password, employeeInfor.Salt);
            return valid;
        }
    }
}
