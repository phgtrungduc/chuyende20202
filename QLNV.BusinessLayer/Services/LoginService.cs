﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Services {
    public class LoginService : BaseService<LoginEntity>, ILoginService {
        ILoginRepository _loginRepository;
        IAuthenticationManager _authenticationManager;
        public LoginService(ILoginRepository loginRepository, IAuthenticationManager authenticationManager) : base(loginRepository) {
            _loginRepository = loginRepository;
            _authenticationManager = authenticationManager;
        }

        public bool ChangePassword(string username, string oldPassword, string newPassword,ServiceResult serviceResult) {
            var loginEntity = _loginRepository.Login(username);
            if (loginEntity != null) {
                var isPasswordOk = CommonFunction.validatePassword(oldPassword, loginEntity.Password, loginEntity.Salt);
                if (isPasswordOk) {
                    var passObject = CommonFunction.hashPassWord(newPassword);
                    var newPass = passObject.GetType().GetProperty("Password").GetValue(passObject);
                    var salt = passObject.GetType().GetProperty("Salt").GetValue(passObject);
                    var updateRows = _loginRepository.UpdatePasswordByEmployeeCode(username,newPass.ToString(),salt.ToString());
                    if (updateRows > 0) {
                        serviceResult.Messenger = "Đổi mật khẩu thành công";
                        serviceResult.StatusCode = Enums.StatusCode.Success;
                        return true;
                    }
                    else {
                        serviceResult.Messenger = "Đổi mật khẩu thất bại";
                        serviceResult.StatusCode = Enums.StatusCode.NotValid;
                        return false;
                    }
                }
                else {
                    serviceResult.Messenger = "Mật khẩu cũ không chính xác";
                    serviceResult.Data = null;
                    serviceResult.StatusCode = Enums.StatusCode.NotValid;
                    return false;
                }
            }
            else {
                serviceResult.Data = null;
                serviceResult.StatusCode = Enums.StatusCode.NotValid;
                serviceResult.Messenger = "Username không tồn tại";
                return false;
            }
            
        }

        public ServiceResult Login(LoginEntity entity, IEmployeeService employeeService) {
            var isValidate = this.Validate(entity);
            if (isValidate == true) {
                
                var loginEntity = _loginRepository.Login(entity.Username);
                //var employee = employeeService.GetEmployeeByCode(entity.Username);
                if (loginEntity == null) {
                    _serviceResult.Messenger = "Username không tồn tại";
                    _serviceResult.Data = null;
                    _serviceResult.StatusCode = Enums.StatusCode.IsValid;
                }
                else {
                    var isPasswordOk = CommonFunction.validatePassword(entity.Password, loginEntity.Password, loginEntity.Salt);
                    if (isPasswordOk) {
                        var accessToken = _authenticationManager.Authenticate(entity.Username, entity.Password);
                        _serviceResult.Messenger = "Đăng nhập thành công";
                        var data = employeeService.GetEmployeeByCode(entity.Username);
                        _serviceResult.Data = new { accessToken = accessToken ,employee= data };
                        _serviceResult.StatusCode = Enums.StatusCode.Success;
                    }
                    else {
                        _serviceResult.Messenger = "Sai mật khẩu";
                        _serviceResult.Data = null;
                        _serviceResult.StatusCode = Enums.StatusCode.IsValid;
                    }
                }
                
                
                //if (res) {
                //    _serviceResult.Messenger = Properties.Resources.Msg_AddSuccess;
                //    _serviceResult.Data = null;
                //    _serviceResult.MISACode = Enums.MISACode.IsValid;
                //}
                //else {
                //    _serviceResult.Messenger = Properties.Resources.Msg_AddFail + _serviceResult.Messenger;
                //    _serviceResult.Data = null;
                //    _serviceResult.MISACode = Enums.MISACode.NotValid;
                //}

                return _serviceResult;
            }
            else {
                return _serviceResult;
            }
        }
    }
}
