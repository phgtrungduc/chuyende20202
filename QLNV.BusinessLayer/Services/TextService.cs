﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Services {
    /// <summary>
    /// Service lấy tất cả cá vị trí
    /// </summary>
    /// CreatedBy:PTDuc(04/12/2020)
    public class TextService : BaseService<Text>, ITextService {
        ITextRepository _textRepository;
        public TextService(ITextRepository textRepository) : base(textRepository) {
            _textRepository = textRepository;
        }

        public ServiceResult GetTextByEmployeeCode(string code) {
            var res = new ServiceResult();
            var text = _textRepository.GetTextByEmployeeCode(code);
            res.Messenger = Properties.Resources.Msg_Success;
            res.Data = text;
            res.StatusCode = Enums.StatusCode.IsValid;
            return res;
        }

        public bool UpdateStatusText(Text text) {
            return _textRepository.UpdateStatusText(text);
        }
    }
}
