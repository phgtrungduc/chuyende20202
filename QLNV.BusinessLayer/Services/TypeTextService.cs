﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace QLNV.BusinessLayer.Services {
    /// <summary>
    /// Service lấy tất cả cá vị trí
    /// </summary>
    /// CreatedBy:PTDuc(04/12/2020)
    public class TypeTextService : BaseService<TypeText>, ITypeTextService {
        ITypeTextRepository _typeTextRepository;
        public TypeTextService(ITypeTextRepository typeTextRepository) : base(typeTextRepository) {
            _typeTextRepository = typeTextRepository;
        }
    }
}
