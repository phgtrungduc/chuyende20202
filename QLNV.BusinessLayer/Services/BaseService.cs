﻿using QLNV.BusinessLayer.Entity;
using QLNV.BusinessLayer.interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using QLNV.BusinessLayer;
using QLNV.BusinessLayer.Enums;

namespace QLNV.BusinessLayer {
    public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : BaseEntity {
        IBaseRepository<TEntity> _baseRepository;
        protected ServiceResult _serviceResult;
        #region Constructor
        public BaseService(IBaseRepository<TEntity> baseRepository) {
            _baseRepository = baseRepository;
            _serviceResult = new ServiceResult() { StatusCode = Enums.StatusCode.Success };
        }
        public virtual bool preAdd(TEntity entity) {
            return true;
        }
        /// <summary>
        /// Thêm đối tượng 
        /// </summary>
        /// <param name="entity">object truyền lên từ client</param>
        /// <returns>1 đối tượng serviceresult bao gồm thông báo,mã lỗi và data</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public  ServiceResult Add(TEntity entity) {
            entity.EntityState = EntityState.AddNew;
            // Thực hiện validate:
            var isValidate = Validate(entity);
            if (isValidate == true ) {
                var row = _baseRepository.Add(entity, _serviceResult);
                if (row != 0) {
                        _serviceResult.Messenger = Properties.Resources.Msg_AddSuccess;
                        _serviceResult.Data = row;
                        _serviceResult.StatusCode = Enums.StatusCode.IsValid;
                    }
                else {
                    _serviceResult.Messenger = Properties.Resources.Msg_AddFail + _serviceResult.Messenger;
                    _serviceResult.Data = row;
                    _serviceResult.StatusCode = Enums.StatusCode.NotValid;
                }

                return _serviceResult;
            }
            else {
                return _serviceResult;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual int AfterAdd() {
            return 1;
        }
        /// <summary>
        /// Xoá đối tượng
        /// </summary>
        /// <param name="entityId">truyền vào id đối tượng cần xóa </param>
        /// <returns></returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public int Delete(Guid entityId) {
            return _baseRepository.Delete(entityId);
        }
        /// <summary>
        /// Lấy tất cả các đối tượng
        /// </summary>
        /// <returns>List các đối tượng</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public IEnumerable<TEntity> GetEntities() {
            return _baseRepository.GetEntities();
        }
        /// <summary>
        /// Tìm kiếm các đối tượng theo id
        /// </summary>
        /// <param name="entityId">id đối tượng</param>
        /// <returns>List các đói tượng có id đã truyền</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public TEntity GetEntityById(Guid entityId) {
            return _baseRepository.GetEntityById(entityId);
        }
        /// <summary>
        /// Update lại thông tin đối tượng
        /// </summary>
        /// <param name="entity">đối tượng với các thông tin update</param>
        /// <returns>Thông báo kết quả</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public ServiceResult Update(TEntity entity) {   
            entity.EntityState = Enums.EntityState.Update;
            var isValidate = Validate(entity);
            if (isValidate == true) {
                var rows = _baseRepository.Update(entity, _serviceResult);
                if (rows > 0) {
                    _serviceResult.Messenger = Properties.Resources.Msg_UpdateSuccess;
                    _serviceResult.StatusCode = StatusCode.IsValid;
                }
                else {
                    _serviceResult.Messenger = Properties.Resources.Msg_Failed + _serviceResult.Messenger;
                    _serviceResult.StatusCode = StatusCode.NotValid;
                }
                return _serviceResult;
            }
            else {
                _serviceResult.Messenger = Properties.Resources.Msg_Failed + _serviceResult.Messenger;
                return _serviceResult;
            }
        }
        #endregion

        /// <summary>
        /// Hàm kiểm tra dữ liệu
        /// </summary>
        /// <param name="entity">object đối tượng cần validate các trường</param>
        /// <returns>true nếu hợp lệ và ngược lại</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        public bool Validate(TEntity entity) {
            var mesArrayError = new List<string>();
            var isValidate = true;
            // Đọc các Property:
            var properties = entity.GetType().GetProperties();

            //Với mỗi thuộc tính, kiểm tra attribute để validate nó
            foreach (var property in properties) {
                var propertyValue = property.GetValue(entity);

                //sử dụng để kiểm tra attribute DisplayName cuả các thuộc tính Customer hay entity
                var displayName = property.GetCustomAttributes(false)
                .OfType<DisplayNameAttribute>()
                .FirstOrDefault(); ;

                // Kiểm tra xem có attribute cần phải validate không:
                if (property.IsDefined(typeof(Required), false)) {
                    // Check bắt buộc nhập:
                    if (propertyValue == null) {
                        isValidate = false;
                        if (displayName != null) {
                            mesArrayError.Add($"Thông tin {displayName.DisplayName} không được phép để trống.");
                        }
                        _serviceResult.StatusCode = Enums.StatusCode.NotValid;
                        _serviceResult.Messenger = Properties.Resources.Msg_NotValidData;
                    }
                }

                if (entity.EntityState == EntityState.AddNew) {
                    if (property.IsDefined(typeof(CheckDuplicate), false)) {
                        // Check trùng dữ liệu:
                        var entityDuplicate = _baseRepository.GetEntityByProperty(entity, property);
                        //entityDuplicate!=null tức là đã tồn tại 1 thằng có giá trị property trùng trên DB
                        if (entityDuplicate != null) {
                            isValidate = false;
                            mesArrayError.Add($"Thông tin {displayName.DisplayName} đã có trên hệ thống.");
                            _serviceResult.StatusCode = Enums.StatusCode.NotValid;
                            _serviceResult.Messenger = Properties.Resources.Msg_NotValidData;
                        }
                    }
                }
                if (property.IsDefined(typeof(MaxLength), false)) {
                    //Lấy độ dài đã khai báo
                    var attributeMaxLength = property.GetCustomAttributes(typeof(MaxLength), true)[0];
                    var length = (attributeMaxLength as MaxLength).Value;
                    var msg = (attributeMaxLength as MaxLength).ErrorMsg;
                    if (propertyValue.ToString().Trim().Length > length) {
                        isValidate = false;
                        mesArrayError.Add(msg ?? $"Thông tin {displayName.DisplayName} vượt quá {length} độ dài cho phép");
                        _serviceResult.StatusCode = Enums.StatusCode.NotValid;
                        _serviceResult.Messenger = Properties.Resources.Msg_NotValidData;
                    }
                }
                //if (property.IsDefined(typeof(Email), false)) {
                //    //Check đúng định dạng email
                //    //var validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                //    //                  + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                //    //                  + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$"; 
                //    var validEmailPattern = @"/^[^\s@]+@[^\s@]+$/";


                //    var regCheck = new Regex(validEmailPattern, RegexOptions.IgnoreCase);
                //    var check = regCheck.IsMatch(propertyValue.ToString().Trim());
                //    if (!check) {
                //        isValidate = false;
                //        mesArrayError.Add(Properties.Resources.Msg_EmailTypeFail);
                //        _serviceResult.MISACode = Enums.MISACode.NotValid;
                //        _serviceResult.Messenger = Properties.Resources.Msg_NotValidData;
                //    }
                //}
            }
            _serviceResult.Data = mesArrayError;
            //Với các lớp con kế thừa, nếu muốn validate thêm các thông tin  
            //sẽ override lại phương thức validateCustom và sau đó thực hiện gì kệ nó
            //nhớ phải gọi this.validate không thì nó chỉ gọi của thằng cha mà không gọi đến của thằng con
            if (isValidate == true) isValidate = this.ValidateCustom(entity);
            return isValidate;
        }
        /// <summary>
        /// Hàm validate cho phép override để thực hiện thêm việc custom cho các nghiệp vụ phía sau 
        /// </summary>
        /// <param name="entity">Đối tượng object</param>
        /// <returns>true/false</returns>
        /// CreatedBy:PTDuc(04/12/2020)
        protected virtual bool ValidateCustom(TEntity entity) {
            return true;
        }

        public AutoIdEntity GetAutoId() {
            return _baseRepository.GetAutoId();
        }

        
    }
}
